import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  salary : number;
  countries = [
    {name :'Poland', value : 'POLAND'},
    {name :'Germany', value : 'GERMANY'},
    {name :'Great Britain', value : 'GREAT BRITAIN'}
  ];

  constructor(private http: HttpClient) {}

  ngOnInit() {}

  onSubmitSalaryForm(formData: { salaryPerDay: number; country: string }) {
    let params = new HttpParams().set("salaryPerDay",formData.salaryPerDay.toString()).set("countryName", formData.country);
    // Send Http request
    this.http
      .get<number>(
        'http://localhost:8080/calculateSalary?',{params : params})
      .subscribe(responseData => {
        this.salary = responseData;
      });
  }
}
