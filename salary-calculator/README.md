﻿## Budowanie i uruchamianie aplikacji
Aplikacje czesci serwerowej mozna uruchomic poprzez uruchomienia klasy SpringBootowej SalaryCalculatorApplication. 
Można także wykonać polecenie mvn clean package, a nastepnie wykonac komende bedac w folderze target
java -jar salarycalculator-1.0.0.jar

## Dostępne serwisy REST
### GET
Ścieżka: '/calculateSalary'

Parametry:
- salaryPerDay – wynagrodzenie dzienne
- countryName - nazwa kraju, w którym jesteśmy na kontrakcie

Przykładowe zapytanie:

'http://localhost:8080/calculateSalary?salaryPerDay=2000&countryName=POLAND'

