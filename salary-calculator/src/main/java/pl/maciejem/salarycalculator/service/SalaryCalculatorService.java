package pl.maciejem.salarycalculator.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.maciejem.salarycalculator.properties.AppProperties;
import pl.maciejem.salarycalculator.properties.CountryCost;
import pl.maciejem.salarycalculator.exchange.ExchangeRates;
import pl.maciejem.salarycalculator.calculator.SalaryCalculator;

import java.math.BigDecimal;

@Service
@Slf4j
@AllArgsConstructor
public class SalaryCalculatorService {

    private ExchangeRates exchangeRates;
    private SalaryCalculator salaryCalculator;
    private AppProperties appProperties;

    public BigDecimal calculateSalary(Double salaryPerDay, String countryName) {

        log.info("Calculating salary (salaryPerDay: {}, countryName: {}", salaryPerDay,
                countryName);
        CountryCost countryCost = appProperties.getCountryCostByCountryName(countryName);
        BigDecimal salaryInBaseCurrency = salaryCalculator.calculateSalary(BigDecimal.valueOf(salaryPerDay), countryCost);
        return exchangeRates.convertCurrencyToPLN(salaryInBaseCurrency, countryCost.getCurrencyCode());
    }
}