package pl.maciejem.salarycalculator.exchange.nbp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@AllArgsConstructor
@Data
@NoArgsConstructor
@ToString
public class ConvertDto implements Serializable {

    private Double amount;
    private String baseCurrency;
    private String targetCurrency;
}
