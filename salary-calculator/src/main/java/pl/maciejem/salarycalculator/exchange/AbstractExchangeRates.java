package pl.maciejem.salarycalculator.exchange;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.support.CronSequenceGenerator;
import pl.maciejem.salarycalculator.exchange.nbp.dto.RateDto;
import pl.maciejem.salarycalculator.properties.AppProperties;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractExchangeRates implements ExchangeRates {

    private RatesDatabase ratesDatabase;

    private AppProperties appProperties;

    @PostConstruct
    public void init(){
        updateRatesIfNeeded();
    }
    public BigDecimal convertCurrencyToPLN(BigDecimal amount, String currencyCode) {
        updateRatesIfNeeded();
        Double targetCurrencyRate = ratesDatabase.getCurrencyRateByCode(currencyCode);
        return amount.multiply(BigDecimal.valueOf(targetCurrencyRate)).setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public void updateRates() {
        log.info("Updating rates");
        List<RateDto> newRates = loadExchangeRates();
        ratesDatabase.setRates(newRates);
        ratesDatabase.setLastUpdate(LocalDateTime.now());
    }

    public abstract List<RateDto> loadExchangeRates();

    public void updateRatesIfNeeded() {
        if (checkIfRatesUpdateIsNeeded()) {
            updateRates();
        }
    }
/* If the Api was not updated by scheduled task, the update should be retried with
each user request. If next update time is bigger than one day it means, that the rates where
not yet updated nor by scheduler nor by another user request
 */
    protected boolean checkIfRatesUpdateIsNeeded() {
        boolean ratesUpdateNeeded = true;
        if (ratesDatabase.getLastUpdate() != null && getNextRatesUpdateDate().isBefore(ratesDatabase.getLastUpdate().plusDays(1))) {
            ratesUpdateNeeded = false;
        }
        return ratesUpdateNeeded;
    }

    protected LocalDateTime getNextRatesUpdateDate() {
        final String cronExpression = appProperties.getLoadExchangeRatesTimeCron();
        final CronSequenceGenerator generator = new CronSequenceGenerator(cronExpression);
        final Date nextExecutionDate = generator.next(new Date());
        return new java.sql.Timestamp(
                nextExecutionDate.getTime()).toLocalDateTime();
    }
}