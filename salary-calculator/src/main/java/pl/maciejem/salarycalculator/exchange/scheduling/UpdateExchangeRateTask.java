package pl.maciejem.salarycalculator.exchange.scheduling;

import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.maciejem.salarycalculator.exchange.ExchangeRates;

/*
This Scheduler updates rates every day at the time specified in properties.
NbpApi updates their rates everyday till 11:30 so the application updates it
right after api gets updated.
 */
@Component
@AllArgsConstructor
public class UpdateExchangeRateTask {

    private ExchangeRates updateExchangeRates;

    @Scheduled(cron = "${app.loadExchangeRatesTimeCron}")
    public void updateExchangeRate() {
        updateExchangeRates.updateRates();
    }
}