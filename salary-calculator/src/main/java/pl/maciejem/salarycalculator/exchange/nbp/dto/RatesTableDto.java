package pl.maciejem.salarycalculator.exchange.nbp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@Data
@NoArgsConstructor
@ToString
public class RatesTableDto implements Serializable {

    private String table;
    @JsonProperty("no") private String number;
    private String effectiveDate;
    private List<RateDto> rates;
}
