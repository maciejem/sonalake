package pl.maciejem.salarycalculator.exchange;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.annotation.ApplicationScope;
import pl.maciejem.salarycalculator.exchange.nbp.dto.RateDto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ApplicationScope
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RatesDatabase implements Serializable {
    private LocalDateTime lastUpdate;
    private List<RateDto> rates = new ArrayList<>();

    public Double getCurrencyRateByCode(String currencyCode) {
        if (StringUtils.isEmpty(currencyCode)) {
            throw new IllegalStateException(String.format("Currency code %s cannot be empty", currencyCode));
        }

        RateDto rate = rates.stream().filter(r -> r.getCode().contentEquals(currencyCode))
                .findFirst().orElse(null);
        if (rate == null) {
            throw new IllegalStateException(String.format("Unknown currency code %s", currencyCode));
        }
        return rate.getMid();
    }
}
