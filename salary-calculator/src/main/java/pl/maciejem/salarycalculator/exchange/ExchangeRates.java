package pl.maciejem.salarycalculator.exchange;

import java.math.BigDecimal;

public interface ExchangeRates {

    BigDecimal convertCurrencyToPLN(BigDecimal amount, String baseCurrencyCode);

    void updateRates();

    void updateRatesIfNeeded();
}
