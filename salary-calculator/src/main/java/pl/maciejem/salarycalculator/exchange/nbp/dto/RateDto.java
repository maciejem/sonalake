package pl.maciejem.salarycalculator.exchange.nbp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@Data
@NoArgsConstructor
@ToString
public class RateDto implements Serializable {

    private String currency;
    private String code;
    private Double mid;
}
