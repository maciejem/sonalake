package pl.maciejem.salarycalculator.exchange.nbp;

import pl.maciejem.salarycalculator.exchange.AbstractExchangeRates;
import pl.maciejem.salarycalculator.exchange.RatesDatabase;
import pl.maciejem.salarycalculator.exchange.nbp.dto.RateDto;
import pl.maciejem.salarycalculator.exchange.nbp.dto.RatesTableDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import pl.maciejem.salarycalculator.properties.AppProperties;

import java.util.ArrayList;
import java.util.List;

@Component
public class NbpExchangeRates extends AbstractExchangeRates {

    public NbpExchangeRates(RatesDatabase ratesDatabase, AppProperties appProperties){
        super(ratesDatabase, appProperties);
    }

    @Override
    public List<RateDto> loadExchangeRates() {
        List<RateDto> exchangeRates = loadExchangeRatesFromTable("A");
        //adding PLN rate for calculating salary
        exchangeRates.add(getPlnRate());
        return  exchangeRates;
    }

    private List<RateDto> loadExchangeRatesFromTable(String tableName) {
        List<RateDto> newRates = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<RatesTableDto>> rateResponse;
        try {
            rateResponse = restTemplate.exchange("http://api.nbp.pl/api/exchangerates/tables/" + tableName,
                    HttpMethod.GET, null, new ParameterizedTypeReference<List<RatesTableDto>>() {});
        } catch (RestClientException ex) {
            throw new IllegalStateException("NBP convert rates api unavailable");
        }

        List<RatesTableDto> ratesTableList = rateResponse.getBody();
        if (!CollectionUtils.isEmpty(ratesTableList)) {
            newRates.addAll(ratesTableList.get(0).getRates());
        }
        return newRates;
    }

    private RateDto getPlnRate() {
        RateDto rate = new RateDto();
        rate.setCurrency("Złoty polski");
        rate.setCode("PLN");
        rate.setMid(1.0);
        return rate;
    }
}