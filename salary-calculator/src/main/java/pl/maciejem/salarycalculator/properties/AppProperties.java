package pl.maciejem.salarycalculator.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties("app")
@Data
public class AppProperties {
    private List<CountryCost> countriesCosts = new ArrayList<>();
    private Integer numberOfWorkingDaysAMonth;
    private String loadExchangeRatesTimeCron;
    private String loadExchangeRatesTime;

    public CountryCost getCountryCostByCountryName(String countryName){
        if (StringUtils.isEmpty(countryName)) {
            throw new IllegalStateException(String.format("Country name %s cannot be empty", countryName));
        }
        CountryCost countryCost = countriesCosts.stream().filter(cc -> cc.getCountryName().equals(countryName))
                .findFirst().orElse(null);
        if (countryCost == null) {
            throw new IllegalStateException(String.format("No data to calculate salary for given country %s", countryName));
        }
        return countryCost;
    }
}