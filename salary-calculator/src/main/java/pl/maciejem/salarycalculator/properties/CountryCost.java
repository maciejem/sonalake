package pl.maciejem.salarycalculator.properties;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CountryCost {
    private String countryName;
    private Double fixedCosts;
    private Double tax;
    private String currencyCode;
}