package pl.maciejem.salarycalculator.calculator;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.maciejem.salarycalculator.properties.AppProperties;
import pl.maciejem.salarycalculator.properties.CountryCost;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
@Slf4j
@AllArgsConstructor
public class SalaryCalculator {

    private AppProperties appProperties;

    public BigDecimal calculateSalary(BigDecimal salaryPerDay, CountryCost countryCost) {
        log.info("Calculating salary salaryPerDay={}, countryCost={}", salaryPerDay, countryCost);
        BigDecimal salaryPerMonthBeforeCostsSubtracted = salaryPerDay.multiply(BigDecimal.valueOf(appProperties.getNumberOfWorkingDaysAMonth()));
        BigDecimal salaryPerMonthSubtractFixedCosts = salaryPerMonthBeforeCostsSubtracted.subtract(BigDecimal.valueOf(countryCost.getFixedCosts()));
        BigDecimal taxAmount = salaryPerMonthSubtractFixedCosts.multiply(BigDecimal.valueOf(countryCost.getTax())).divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);
        return salaryPerMonthSubtractFixedCosts.subtract(taxAmount);
    }
}
