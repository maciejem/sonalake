package pl.maciejem.salarycalculator.web;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import pl.maciejem.salarycalculator.service.SalaryCalculatorService;

import java.math.BigDecimal;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/calculateSalary")
@AllArgsConstructor
public class SalaryCalculatorController {

    private SalaryCalculatorService salaryCalculatorService;

    @GetMapping()
    public BigDecimal calculateSalary(
            @RequestParam(name = "salaryPerDay") Double salaryPerDay,
            @RequestParam(name = "countryName") String countryName) {
        return salaryCalculatorService.calculateSalary(salaryPerDay, countryName);
    }

    @ExceptionHandler(NumberFormatException.class)
    public String numberFormatExceptionHandler() {
        return "Given salary per day must be a number";
    }

    @ExceptionHandler(IllegalStateException.class)
    public String illegalStateExceptionHandler(IllegalStateException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(RestClientException.class)
    public String restClientExceptionHandler(IllegalStateException ex) {
        return ex.getMessage();
    }
}