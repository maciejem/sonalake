package pl.maciejem.salarycalculator.exchange;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Java6Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)

public class SalaryCalculatorIntegrationTest {

	private static final String APP_HOST = "/calculateSalary?salaryPerDay={salaryPerDay}&countryName={countryName}";

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void shouldReturnSalaryForGermany() {
		Double salaryPerDay = 100d;
		String countryName= "GERMANY";
		BigDecimal salary = this.restTemplate.getForObject(APP_HOST, BigDecimal.class, salaryPerDay, countryName);
		assertThat(salary).isNotNull();
		assertThat(salary).isGreaterThan(BigDecimal.ZERO);
	}
	@Test
	public void shouldReturnSalaryForPoland() {
		Double salaryPerDay = 100d;
		String countryName= "POLAND";
		BigDecimal salary = this.restTemplate.getForObject(APP_HOST, BigDecimal.class, salaryPerDay, countryName);
		assertThat(salary).isNotNull();
		assertThat(salary).isGreaterThan(BigDecimal.ZERO);
	}
}