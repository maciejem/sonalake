package pl.maciejem.salarycalculator.exchange;

import pl.maciejem.salarycalculator.properties.AppProperties;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import pl.maciejem.salarycalculator.properties.CountryCost;
import pl.maciejem.salarycalculator.calculator.SalaryCalculator;

import java.math.BigDecimal;


@RunWith(MockitoJUnitRunner.class)
public class SalaryCalculatorTest {

    @InjectMocks
    private SalaryCalculator salaryCalculator;

    @Mock
    private AppProperties appProperties;

    @Test
    public void calculateSalaryForGermanySuccessfulTest() {
        //given
        CountryCost countryCost = CountryCost.builder().countryName("GERMANY").fixedCosts(1200d).tax(20d).build();
        Mockito.when(appProperties.getNumberOfWorkingDaysAMonth()).thenReturn(22);
        BigDecimal salaryPerDay = new BigDecimal("100");
        //when
        BigDecimal salary = salaryCalculator.calculateSalary(salaryPerDay, countryCost);
        //then
        Assertions.assertThat(salary).isEqualTo(new BigDecimal("800.00"));
    }

    @Test
    public void calculateSalaryForPolandSuccessfullTest() {
        //given
        CountryCost countryCost = CountryCost.builder().countryName("POLAND").fixedCosts(1200d).tax(19d).build();
        Mockito.when(appProperties.getNumberOfWorkingDaysAMonth()).thenReturn(22);
        BigDecimal salaryPerDay = new BigDecimal("100");
        //when
        BigDecimal salary = salaryCalculator.calculateSalary(salaryPerDay, countryCost);
        //then
        Assertions.assertThat(salary).isEqualTo(new BigDecimal("810.00"));
    }


    @Test
    public void calculateSalaryForDecimalSalaryPerDayTest() {
        //given
        CountryCost countryCost = CountryCost.builder().countryName("POLAND").fixedCosts(1200d).tax(19d).build();
        Mockito.when(appProperties.getNumberOfWorkingDaysAMonth()).thenReturn(22);
        BigDecimal salaryPerDay = new BigDecimal("100.50");
        //when
        BigDecimal salary = salaryCalculator.calculateSalary(salaryPerDay, countryCost);
        //then
        Assertions.assertThat(salary).isEqualTo(new BigDecimal("818.91"));
    }
}
