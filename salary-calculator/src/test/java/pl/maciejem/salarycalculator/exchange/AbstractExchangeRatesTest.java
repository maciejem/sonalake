package pl.maciejem.salarycalculator.exchange;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import pl.maciejem.salarycalculator.exchange.nbp.NbpExchangeRates;
import pl.maciejem.salarycalculator.properties.AppProperties;

import java.time.LocalDateTime;

@RunWith(MockitoJUnitRunner.class)
public class AbstractExchangeRatesTest {

    @InjectMocks
    private NbpExchangeRates abstractExchangeRates;

    @Mock
    private RatesDatabase ratesDatabase;

    @Mock
    private AppProperties appProperties;

    @Test
    public void checkIfRatesUpdateIsNeededTest() {
        //given
        Mockito.when(appProperties.getLoadExchangeRatesTimeCron()).thenReturn("0 30 11 * * *");
        LocalDateTime nextRatesUpdateDate= abstractExchangeRates.getNextRatesUpdateDate();
        LocalDateTime lastUpdateDate = nextRatesUpdateDate.minusDays(2);
        Mockito.when(ratesDatabase.getLastUpdate()).thenReturn(lastUpdateDate);
        //when
        boolean ratesUpdateIsNeeded = abstractExchangeRates.checkIfRatesUpdateIsNeeded();
        //then
        Assertions.assertThat(ratesUpdateIsNeeded).isTrue();
    }

    @Test
    public void checkIfRatesUpdateIsNotNeededTest() {
        //given
        Mockito.when(appProperties.getLoadExchangeRatesTimeCron()).thenReturn("0 30 11 * * *");
        LocalDateTime nextRatesUpdateDate= abstractExchangeRates.getNextRatesUpdateDate();
        LocalDateTime lastUpdateDate = nextRatesUpdateDate.minusHours(1);
        Mockito.when(ratesDatabase.getLastUpdate()).thenReturn(lastUpdateDate);
        //when
        boolean ratesUpdateIsNeeded = abstractExchangeRates.checkIfRatesUpdateIsNeeded();
        //then
        Assertions.assertThat(ratesUpdateIsNeeded).isFalse();
    }
}